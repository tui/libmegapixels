#pragma once

void
init_log(int level);

void
log_error(const char *fmt, ...);

void
log_debug(const char *fmt, ...);